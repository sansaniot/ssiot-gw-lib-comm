package setting

import (
	"github.com/spf13/viper"
)

// 定义一个全局的配置对象(其他模块获取)
var Env *viper.Viper

//配置启动参数key
var _configTypeKey = "config"

// Start 初始化系统配置文件
func Start() {
	//初始化系统配置文件参数信息
	initSystemConfig()
}

// 初始化系统配置文件参数
func initSystemConfig() {
	//创建读取配置对象
	config := viper.New()
	//获取指定的配置文件和路径
	configPath := GetConfigPath()
	//配置文件路径
	config.SetConfigFile(configPath)
	//设置初始化默认参数
	initDefaultConfig(config)
	//尝试进行配置读取
	if err := config.ReadInConfig(); err != nil {
		// 配置文件未找到错误；如果需要可以忽略
		panic(err)
	} else {
		//读取成功
		Env = config
	}
}

// 初始化系统默认参数
func initDefaultConfig(config *viper.Viper) {
	//设置默认日志保存路径
	//config.SetDefault("settings.logger.path", "temp/logs")
	//设置日志默认级别
	config.SetDefault("ssiot.log.level", "info")
}

// 获取系统的读取的配置文件
func GetConfigPath() string {
	//获取启动类型
	settingPath := viper.GetString(_configTypeKey)
	//动态获取读取的文件名称
	if settingPath == "" {
		//设置默认类型的配置文件
		settingPath = "./config/settings.yml"
	}
	//返回
	return settingPath
}
