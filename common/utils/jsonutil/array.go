package jsonutil

import (
	"encoding/json"
	"fmt"
)

func Json2Array(str string) []interface{} {
	var tempArray = make([]interface{}, 0)
	err := json.Unmarshal([]byte(str), &tempArray)
	if err != nil {
		fmt.Printf("Json to array error ", err)
		//panic(err)
	}
	return tempArray
}

// Json2ByteArray Json数组转为byte数组
func Json2ByteArray(data []interface{}) []byte {
	data1 := make([]byte, 0)
	for _, d := range data {
		if _, ok := d.(float64); ok {
			data1 = append(data1, byte(d.(float64)))
		}
	}
	return data1
}
