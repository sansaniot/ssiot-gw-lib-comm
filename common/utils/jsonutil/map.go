package jsonutil

import (
	"encoding/json"
	"fmt"
	"github.com/valyala/fastjson"
)

func Json2Map(str string) map[string]interface{} {
	var tempMap = make(map[string]interface{})
	err := json.Unmarshal([]byte(str), &tempMap)
	if err != nil {
		fmt.Printf("json to map error: ", err)
		//panic(err)
	}
	return tempMap
}

func Byte2Map(bytes []byte) map[string]interface{} {
	var tempMap = make(map[string]interface{})
	err := json.Unmarshal(bytes, &tempMap)
	if err != nil {
		fmt.Printf("json to map error: ", err)
		//panic(err)
	}
	return tempMap
}

func Map2Byte(m interface{}) []byte {
	b, err := json.Marshal(m)
	if err != nil {
		fmt.Printf("map to byte error: ", err)
	}
	return b
}

func JoinMap(map1, map2 map[string]interface{}) map[string]interface{} {
	for _, _ = range map1 {
		for k2, _ := range map2 {
			map1[k2] = map2[k2]
		}
	}
	return map1
}

func FastJsonParse(str string) *fastjson.Value {
	var p fastjson.Parser
	v, err := p.Parse(str)

	if err != nil {
		fmt.Printf("fastJson decode error: ", err)
	}

	return v
}
