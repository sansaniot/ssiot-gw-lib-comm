package files

import (
	"fmt"
	"os"
)

// HasDir 判断文件夹是否存在
func HasDir(path string) (bool, error) {
	_, _err := os.Stat(path)

	if _err == nil {
		return true, nil
	}

	if os.IsNotExist(_err) {
		return false, nil
	}

	return false, _err
}

// CreateDir 创建文件夹
func CreateDir(path string) {
	_exist, _err := HasDir(path)

	if _err != nil {
		fmt.Printf("Check dir error: %v\n", _err)
		return
	}

	if _exist {
		return
	} else {
		err := os.Mkdir(path, os.ModePerm)
		err1 := os.Chmod(path, os.ModePerm)

		if err != nil || err1 != nil {
			fmt.Printf("Make dir error: %v\n", err)
		} else {
			return
		}
	}
}
