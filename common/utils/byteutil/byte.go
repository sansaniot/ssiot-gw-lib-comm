package byteutil

import (
	"bytes"
	"encoding/binary"
	"math"
)

// Int32ToBytes 整形转换成字节
func Int32ToBytes(n int) []byte {
	x := int32(n)
	bytesBuffer := bytes.NewBuffer([]byte{})
	binary.Write(bytesBuffer, binary.BigEndian, x)
	return bytesBuffer.Bytes()
}

// Int16ToBytes 整形转换成字节
func Int16ToBytes(n int) []byte {
	x := int16(n)
	bytesBuffer := bytes.NewBuffer([]byte{})
	binary.Write(bytesBuffer, binary.BigEndian, x)
	return bytesBuffer.Bytes()
}

// BytesToInt 字节转换成整形
func BytesToInt(b []byte) int {
	x := binary.BigEndian.Uint16(b)
	return int(x)
}

// ByteSliceToInt byte数组转为int16数组
func ByteSliceToInt(s []byte) (s1 []int16) {
	s1 = make([]int16, 0)
	for _, value := range s {
		s1 = append(s1, int16(value))
	}
	return
}

func BytesCombine(pBytes ...[]byte) []byte {
	length := len(pBytes)
	s := make([][]byte, length)
	for index := 0; index < length; index++ {
		s[index] = pBytes[index]
	}
	sep := []byte("")
	return bytes.Join(s, sep)
}

//Float64ToByte Float64转byte
func Float64ToByte(float float64) []byte {
	bits := math.Float64bits(float)
	bytes := make([]byte, 8)
	binary.LittleEndian.PutUint64(bytes, bits)
	return bytes
}

//ByteToFloat64 byte转Float64
func ByteToFloat64(bytes []byte) float64 {
	bits := binary.LittleEndian.Uint64(bytes)
	return math.Float64frombits(bits)
}

func Float32ToByte(float float32) []byte {
	bits := math.Float32bits(float)
	bytes := make([]byte, 4)
	binary.LittleEndian.PutUint32(bytes, bits)
	return bytes
}

func ByteToFloat32(bytes []byte) float32 {
	bits := binary.LittleEndian.Uint32(bytes)
	return math.Float32frombits(bits)
}

//小端序 直接字节数(低字节在后 高字节在前)
func ChangeLitEndBytesSize(dataBytes []byte, c int) []byte {
	//当前长度
	len := len(dataBytes)
	//处理
	if len > c {
		//切割
		return dataBytes[:c]
	} else {
		//添加多余的
		emptyByte := make([]byte, c-len)
		//返回
		return append(dataBytes, emptyByte...)
	}
}

//大端序 直接字节数(低字节在前 高字节在后)
func ChangeBigEndBytesSize(dataBytes []byte, c int) []byte {
	//当前长度
	len := len(dataBytes)
	//处理
	if len > c {
		//切割
		return dataBytes[(len - c):]
	} else {
		//添加多余的
		emptyByte := make([]byte, c-len)
		//返回
		return append(emptyByte, dataBytes...)
	}
}

//改变字节顺序
func ChangeBytesOrder(dataBytes []byte) []byte {
	//修改
	len := len(dataBytes)
	newData := make([]byte, len)
	//处理
	for i := 0; i < len; i++ {
		//缓存
		newData[i] = dataBytes[(len-1)-i]
	}
	//返回
	return newData
}

//改变字节顺序(小端格式)
func ChangeIntToBytes(val int, count int) []byte {
	x := int16(val)
	bytesBuffer := bytes.NewBuffer([]byte{})
	binary.Write(bytesBuffer, binary.LittleEndian, x)
	dataBytes := bytesBuffer.Bytes()
	return ChangeLitEndBytesSize(dataBytes, count)
}

//改变字节顺序(小端格式)
func ChangeFloat32ToBytes(val float32, count int) []byte {
	bits := math.Float32bits(val)
	bytes := make([]byte, count)
	binary.LittleEndian.PutUint32(bytes, bits)
	return bytes
}
