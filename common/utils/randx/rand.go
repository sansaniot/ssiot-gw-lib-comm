package randx

import "time"
import "math/rand"

func Randint() int {
	rand.Seed(time.Now().UnixNano()) //以当前系统时间作为种子参数
	return rand.Intn(1000)
}
