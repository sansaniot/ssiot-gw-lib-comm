package syncMap

import (
	"net"
	"sync"
)

type SyncMap struct {
	m sync.Map
}

func (s *SyncMap) Load(key any) (value any, ok bool) {
	var load any
	load, ok = s.m.Load(key)

	if ok {
		value = load
	} else {
		value = nil
	}

	return
}

func (s *SyncMap) Store(key any, value any) {
	s.m.Store(key, value)
}

func (s *SyncMap) LoadOrStore(key any, value any) (actual any, loaded bool) {
	var store any
	store, loaded = s.m.LoadOrStore(key, value)

	if store != nil {
		actual = store
	}

	return
}

func (s *SyncMap) Delete(key any) {
	s.m.Delete(key)
}

func (s *SyncMap) Range(f func(key any, value any) bool) {
	f1 := func(key, value any) bool {
		return f(key.(string), value.(net.Conn))
	}
	s.m.Range(f1)
}
