package crc

import (
	"fmt"
	"testing"
)

func TestCrc(t *testing.T) {
	m_data := []byte{0x01, 0x02, 0x03, 0x04}
	modbusData := ModbusCrc(m_data)
	fmt.Printf("check sum:%X \n", modbusData)
}
