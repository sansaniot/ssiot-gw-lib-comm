package global

import mqtt "github.com/eclipse/paho.mqtt.golang"

// MqttClient 全局的mqtt client
var MqttClient mqtt.Client
