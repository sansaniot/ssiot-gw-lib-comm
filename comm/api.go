package comm

import (
	"gitee.com/sansaniot/ssiot-gw-lib-comm/comm/common"
	"gitee.com/sansaniot/ssiot-gw-lib-comm/comm/types"
)

func SetOnConnectCallBack(onConnect types.OnConnectFunc) {
	common.OnConnect = onConnect
}

func SetOnDisconnectCallBack(onDisConnect types.OnDisconnectFunc) {
	common.OnDisconnect = onDisConnect
}

func SetCreatTaskCallBack(createTaskAfterConnect types.CreateTaskAfterConnectFunc) {
	common.CreateTaskAfterConnect = createTaskAfterConnect
}

func SetDestroyTaskCallBack(destroyTaskAfterDisConnect types.DestroyTaskAfterDisconnectFunc) {
	common.DestroyTaskAfterDisconnect = destroyTaskAfterDisConnect
}

func SetReceiveDataCallBack(receiveData types.ReceiveDataFunc) {
	common.OnReceiveData = receiveData
}

func SendCommand(command *types.ProbeCommand) {
	commandChannelAny, _ := types.ProbeCommandChannels.Load(command.CommId)
	if commandChannelAny == nil {
		return
	}
	commandChannel := commandChannelAny.(chan *types.ProbeCommand)
	commandChannel <- command
}
