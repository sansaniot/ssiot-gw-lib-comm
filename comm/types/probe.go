package types

import (
	"sync"
)

// ProbeCommandChannels 接收采集命令的channel
// map[string]chan *ProbeCommand
var ProbeCommandChannels sync.Map

// ProbeDataChannels DPU库中回调的结果放到通道中，用于组装上报数据
// map[string]chan map[string]interface{}
var ProbeDataChannels sync.Map

// DpuTask 当前云网关是否有dpu配置
var DpuTask sync.Map

// ProbeCommand 采集命令
type ProbeCommand struct {
	Command []byte
	CommId  string
}
