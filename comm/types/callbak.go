package types

type (
	OnConnectFunc                  func(devSn string)
	OnDisconnectFunc               func(devSn string)
	CreateTaskAfterConnectFunc     func(commId string, config string) error
	DestroyTaskAfterDisconnectFunc func(commId string) error
	ReceiveDataFunc                func(commId string, data []byte)
)
