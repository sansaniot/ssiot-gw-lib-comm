package comm

import (
	"gitee.com/sansaniot/ssiot-gw-lib-comm/comm/tcp"
	"gitee.com/sansaniot/ssiot-gw-lib-comm/common/db"
	"gitee.com/sansaniot/ssiot-gw-lib-comm/common/utils/timeticker"
	"gitee.com/sansaniot/ssiot-gw-lib-comm/model"
	"time"
)

func Init() {
	db.Start()
	initTables()
	startJobs()
}

func startJobs() {
	// 采集任务
	var probeMgr *timeticker.TimeTicker = timeticker.New(5 * time.Second)
	probeMgr.AddJob(tcp.ManageProbes, true)
	go probeMgr.Start()
}

func initTables() {
	err := db.Db.AutoMigrate(&model.Device{}, &model.DpuConfig{})
	if err != nil {
		panic("failed to init database tables")
	}
}
