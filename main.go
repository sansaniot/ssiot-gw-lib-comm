package main

import (
	"gitee.com/sansaniot/ssiot-gw-lib-comm/comm"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	comm.Init()

	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, syscall.SIGINT, syscall.SIGTERM)
	<-sigc
	println("exit OK")
}
